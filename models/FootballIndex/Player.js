const mongoose = require('mongoose');
const slugify = require('slugify');
const Stock = require('./Stocks');

const PlayerSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: [true, 'Please add a name']
    },
    slug: String,
    description: {
        type: String,
        maxlength: [500, 'Description can not be more than 500 char']
    },
    photo: {
        type: String,
        default: 'no-photo.jpg'
    },
    team: {
        type: String,
        trim: true
    },
    rating: {
        type: Number
    },
    createdAt : {
        type: Date,
        default: Date.now
    },
  },  {
        toJSON: {virtuals : true},
        toObject: {virtuals : true}
})

PlayerSchema.pre('save', function (next) {
    console.log('Slugify ran', this.name)
    this.slug = slugify(this.name, { lower:true });
    next();
})




// Revers populate with virtuals
PlayerSchema.virtual('stocks', {
    ref: 'Stock',
    localField: '_id',
    foreignField: 'player',
    justOne: false
})

PlayerSchema.virtual('tweets', {
    ref: 'Twitter',
    localField: '_id',
    foreignField: 'player',
    justOne: false
})

module.exports = mongoose.model('Player', PlayerSchema );