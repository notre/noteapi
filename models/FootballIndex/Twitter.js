const mongoose = require('mongoose');
const Player = require('../../models/FootballIndex/Player')

const TwitterSchema = new mongoose.Schema({
    player: {
        type: mongoose.Schema.ObjectId,
        ref: 'Player',
        required: true
    },
    date : {
        type: Date,
        required: [false, 'Please add a date']
    },
    username :{
        type: String
    },
    to: {
        type: String
    },
    text: {
        type: String
    } ,
    retweets: {
        type: Number
    } ,
    favorites: {
        type: Number
    },
    replies: {
        type: Number
    },
    user: {
        type: String
    },
    id: {
        type: Number
    },
    permalink: {
        type: String
    } ,
    author_id: {
        type: Number
    } ,
    formatted_date: {
        type: String
    },
    hashtags: {
        type: String
    },
    mentions: {
        type: String
    },
    geo: {
        type: String
    },
    urls: {
        type: String
    },
    name: {
        type: String
    },
    createdAt : {
        type: Date,
        default: Date.now
    }
})

// TODO: Test this
TwitterSchema.pre('save', async function(next){
    if(this.player){
        next();
    }

    

    // Find player in DB
    const player = await Player.findOne({name: this.name})  

    
    // Setting the object id 
    this.player = player._id     
    next();
});

module.exports = mongoose.model('Twitter', TwitterSchema );