const mongoose = require('mongoose');

const StocksSchema = new mongoose.Schema({

    buy: {
        type: Number,
        required: [true, 'Please add a buy value']
    },
    sell: {
        type: Number,
        required: [true, 'Please add a sell value']
    },
    date: {
        type: Date,
        required: [false, `No date what added, thus todays date and time will be assinged ${Date.now}`]
    },
    createdAt : {
        type: Date,
        default: Date.now
    },    
    player: {
        type: mongoose.Schema.ObjectId,
        ref: 'Player',
        required: true
    }
})





module.exports = mongoose.model('Stock', StocksSchema );