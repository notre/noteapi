const advancedResults = (model, populate) => async (req, res, next) => {
    let query;
    // Copy req.query
    const reqQuary = { ...req.query};

    // Feilds to exlude
    const resmoveFields = ['select', 'sort', 'page', 'limit'];

    // Loop over remove Feilds and delete them from reqQuery
    resmoveFields.forEach(param => delete reqQuary[param]);


    // For more advanced querys to work like grater than ect. Mongo db requers $ to be in cretain places, express does not hadle this for use.
    let queryStr = JSON.stringify(reqQuary);
    queryStr = queryStr.replace(/\b(gt|gte|lt|lte|in)\b/g, match => `$${match}`);

    // finding resource  (the populate this gets alll the courses I used mongo virtual to revers populate)
    query = model.find(JSON.parse(queryStr));


    // select querys and populate relevent feilds
    if(req.query.select){
        const fields = req.query.select.split(',').join(' ');
        if(populate){
            popArr = populate.split(' ');
            selectArr = req.query.select.split(','); 
            temp = popArr.filter(element => selectArr.includes(element)).join(' ');
            query = query.populate(temp).select(fields); 
        }else{
            query = query.select(fields); 
        }
    }else{
        if(populate){
            query = query.populate(populate); 
        }
    }

    // Sort
    if(req.query.sort){
        // this will sort the req
        const sortBy = req.query.sort.split(',').join(' ');
        query = query.sort(sortBy);
    }else{
        // default will be sorted by the date
        query = query.sort('-createdAt')
    }

    // pagination
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.limit, 10) || 200;
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;
    const total = await model.countDocuments()


    query = query.skip(startIndex).limit(limit);



    

    // The req.quary is key value pairs to add to the search in the DB. See more in README
    // exicuting the query
    const results = await query;
    // console.log(query)

    const pagination = {}
    if(endIndex < total){
        pagination.next = {
            page: page + 1,
            limit
        }
    }
    
    if(startIndex > 0){
        pagination.prev = {
            page: page - 1,
            limit
        }
    }

    res.advancedResults = {
        success: true,
        count: results.length,
        pagination,
        data: results
    }

    next();
}

module.exports = advancedResults;