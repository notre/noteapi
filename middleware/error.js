const ErrorResponse = require('../utils/errorResponse');

const errorHandler = (err, req, res, next) => {

    let error = { ...err }

    error.message = err.message;
    // Log to console for dev

    //Mongoose bad Object ID
    if(err.name === 'CastError'){
        const message = `Player not found id of ${err.value}`;
        error = new ErrorResponse(message, 404);
    }

    // Mongoose duplicat key
    if(err.code === 11000){
        const message = `Duplicate feild value entered`;
        error = new ErrorResponse(message, 400);
    }

    // Mongoose validation errors
    if(err.name === 'ValidationError'){
        // This is mapping through all the val errors added in models when defining the db schema. Sends them in the response message
        const message = Object.values(err.errors).map(val => val.message);
        error = new ErrorResponse(message, 400 );
    }

    res.status(error.statusCode || 500).json({
        success: false,
        error: error.message || 'Server Error'
    });

};

module.exports = errorHandler;