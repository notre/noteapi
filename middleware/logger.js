// Middleware logger will return what hit the api
// Not using... just an example to show how to log using middleware 
// using logger called morgan

// @desc    Logs request to console
const logger = (req, res, next ) => { 
    console.log(`${req.method} ${req.protocol}://${req.get('host')}${req.originalUrl}`)
    next();
}

app.use(logger);

module.exports = logger