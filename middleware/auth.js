const jwt = require('jsonwebtoken');
const asyncHandler = require('./async');
const ErrorResponse = require('../utils/errorResponse');
const User = require('../models/FootballIndex/Users');

// Protect routes
exports.protect = asyncHandler(async( req, res, next ) => {
    let token;

    if(req.headers.authorization && req.headers.authorization.startsWith('Bearer')){
        token = req.headers.authorization.split(' ')[1];
    }else if (req.cookies.token){
        token = req.cookies.token;
    }

    // Make suer token exits
    if(!token){
        return next(new ErrorResponse('Not authorize to access this route', 401));
    }

    // Verify token
    try {
        const decoded = jwt.verify(token, 'lakjsdfkjnjsdjasndv');

        console.log(decoded);

        req.user = await User.findById(decoded.id);
        next();
    } catch (err) {
        return next(new ErrorResponse('Not authorize to access this route', 401));
    }
})