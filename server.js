const path = require('path');
const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const connectDB = require('./config/db');
const errorHandler = require('./middleware/error');
const colors = require('colors');
const rateLimit = require('express-rate-limit')
const fileupload = require('express-fileupload');
const cookieParser = require('cookie-parser');
const helmet = require('helmet');

// Load env vars
dotenv.config({path: './config/config.env'});

//Connect to db
connectDB();

const auth = require('./routes/FootballIndex/auth');
const players = require('./routes/FootballIndex/players');
const stocks = require('./routes/FootballIndex/stocks');
const twitter = require('./routes/FootballIndex/twitter');


const app = express();

// Body parser
app.use(express.json());

// Cookie parser
app.use(cookieParser());

//Rate limits - not using but could be good for future
const limiter = rateLimit({
    windowMs: 10 * 60 * 1000,   // 10 mins
    max: 1 // The max requets
});

// app.use(limiter)


// Dev loggin middleware
if(process.env.NODE_ENV === 'development'){
    app.use(morgan('dev'));
}

app.use(fileupload());

app.use(helmet())

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));


//Mount routers (THIS ADDS TO THE PLAYER API URL!!! )
app.use('/api/v1/footballindex/players', players);
app.use('/api/v1/footballindex/stocks', stocks);
app.use('/api/v1/footballindex/tweets', twitter);
app.use('/api/v1/footballindex/auth', auth);
app.use(errorHandler);


// process.env.$VAR_IN_CONFIG this is how we can access vars thoughout the project
const PORT = process.env.PORT || 3000

const server = app.listen(PORT, console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold));


// Handle unhanded rejections
process.on('unhandledRejection', (err, promise) => {
    console.log(`ERROR ${err.message}`.red);
    // Close and exit
    server.close(() => {
        process.exit(1);
    });
})



