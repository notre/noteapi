const Player = require('../../models/FootballIndex/Player')
const path = require('path');
const asyncHandler = require('../../middleware/async');
const ErrorResponse = require('../../utils/errorResponse');
const Stock = require('../../models/FootballIndex/Stocks');



// @desc    Get all players
// @route   GET /api/v1/footballindex/players
// @access  Public
exports.getPlayers= asyncHandler(async (req, res, next) => {
    res.status(200).json(res.advancedResults);
});


// @desc    Get single player
// @route   GET /api/v1/footballindex/player/:id
// @access  Public
exports.getPlayer = asyncHandler(async(req, res, next) => {
    const player = await Player.findById(req.params.id);
    // This is cos if you give url a correctly formated id it will return a 200
    if(!player){
        return next(
            new ErrorResponse(`Player not found with id of ${req.params.id}`, 404)
        )
    }
    res.status(200).json({success: true, data: player});
})



// @desc    Get single player
// @route   GET /api/v1/footballindex/player/:id
// @access  Public
exports.getPlater = asyncHandler(async(req, res, next) => {
    const player = await Player.findById(req.params.id);
    // This is cos if you give url a correctly formated id it will return a 200
    if(!player){
        return next(
            new ErrorResponse(`Player not found with id of ${req.params.id}`, 404)
        )
    }
    res.status(200).json({success: true, data: player});
})

// @desc    Create new Player
// @route   POST /api/v1/footballindex/player
// @access  Private
exports.createPlayer = asyncHandler(async (req, res, next) => {
    // Add user to req.body
    req.body.user = req.user.id;

    // Check for admin privlages 

    if(req.user.role !== 'admin'){
        return next(new ErrorResponse(`The user with id: ${req.user.id} does not have admin pivlages`, 400));
    }

    const player = await Player.create(req.body);

    res.status(201).json({
        success: true,
        data: player 
    })
})

// @desc    Update player
// @route   PUT /api/v1/footballindex/player/:id
// @access  Private
exports.updatePlayer = asyncHandler(async (req, res, next) => {
    let player = await Player.findById(req.params.id);
    // This is cos if you give url a correctly formated id it will return a 200
    
    if(!player){
        return next(
            new ErrorResponse(`Player not found with id of ${req.params.id}`, 404)
        );
    }

    // Make sure user is admin
    if(req.user.role !== 'admin'){
        return next(
            new ErrorResponse(`User is not auth to update this player ${req.params.id}`, 404)
        );
    }

    player = await Player.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true
    });

    res.status(200).json({success: true, data: player});
})



// @desc    Delete player 
// @route   DELETE /api/v1/footballindex/player/:id
// @access  Private
exports.deletePlayer = asyncHandler(async (req, res, next) => {
    const player = await Player.findById(req.params.id);
    // This is cos if you give url a correctly formated id it will return a 200
    if(!player){
        return next(
            new ErrorResponse(`Player not found with id of ${req.params.id}`, 404)
        );
    }

    // Make sure user is admin
    if(req.user.role !== 'admin'){
        return next(
            new ErrorResponse(`User is not auth to delete this Player ${req.params.id}`, 404)
        );
    }
    // need to sperate out the findById and delete so the middlware will be able to run
    player.remove();
    res.status(200).json({success: true, data: {}});
})


// @desc    Uplaod photo for player 
// @route   PUT /api/v1/footballindex/player/:id/photo
// @access  Private
exports.playerPhotoUpload = asyncHandler(async (req, res, next) => {

    const player = await Player.findById(req.params.id);

    if(!player){
        return next(
            new ErrorResponse(`Player not found with id of ${req.params.id}`, 404)
        );
    }

    if(!req.files){
        return next(new ErrorResponse(`Please upload a file`, 400));
    }
    console.log(req.files.file)

    const file = req.files.file

    // Make sure the image is a photo
    if (!file.mimetype.startsWith('image')){
        return next(new ErrorResponse(`Please upload an image file`, 400 ));
    }

    // Check file size
    if(file.size > process.env.MAX_FILE_UPLOAD){
        return next(new ErrorResponse(`Please upload an image less than ${process.env.MAX_FILE_UPLOAD}`, 400 ));
    }

    // Create uniqe filename
    file.name = `photo_${player._id}${path.parse(file.name).ext}`;

    file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async err => {
        if(err){
            return next(new ErrorResponse(`Problem with file upload`, 500 )); 
        }
        await Player.findByIdAndUpdate(req.params.id, {photo: file.name})
        res.status(200).json({
            success: true,
            data: file.name 
        });
    })

    
})



// @desc    Get player stock values
// @route   GET /api/v1/footballindex/player/:id/stockvalues
// @access  Public
exports.getPlayerStockValues = asyncHandler(async(req, res, next) => {
    const player = await Player.findById(req.params.id);
    // This is cos if you give url a correctly formated id it will return a 200
    if(!player){
        return next(
            new ErrorResponse(`Player not found with id of ${req.params.id}`, 404)
        )
    }
    const stock = await Stock.find({player: req.params.id})
    // check if the player has any stock
    if(!stock){
        return next(
            new ErrorResponse(`Player does not have any stock`, 404)
        )
    } 

    const data = Stock.aggregate(
        [
          {
            $group:
              {
                avgBuy: { $avg: "$buy" },
                avgSell: { $avg: "$sell" },
                maxBuy:{ $max: "$buy" },
                maxSell: { $max: "$sell" },
                minBuy: { $min: "$buy" },
                minSell: { $min: "$sell" }
              }
          }
        ]
     )

    res.status(200).json({success: true, data});
})