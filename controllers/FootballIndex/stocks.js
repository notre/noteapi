const Stock = require('../../models/FootballIndex/Stocks');
const asyncHandler = require('../../middleware/async');
const ErrorResponse = require('../../utils/errorResponse');
const Player = require('../../models/FootballIndex/Player')

// @desc    Get all stocks
// @route   GET /api/v1/footballindex/stock
// @route   GET /api/v1/footballindex/players/:playerId/stocks
// @access  Public
exports.getStocks = asyncHandler(async (req, res, next) => {
    if(req.params.playerId){
        const stocks = await Stock.find({player : req.params.playerId})
        return res.status(200).json({
            success: true,
            count: stocks.length,
            data: stocks
        })
    }else{
        res.status(200).json(res.advancedResults);
    }
});

// @desc    Get a single stock
// @route   GET /api/v1/footballindex/stock/:id
// @access  Public
exports.getStock = asyncHandler(async (req, res, next) => {
    const stock = await Stock.findById(req.params.id).populate({
        path: 'player',
        select: 'name buy sell date'
    });

    if(!stock){
        return next(new ErrorResponse(`No stock with the id of ${req.params.id}`), 404);
    }

    res.status(200).json({success: true, data: stock});
})


// @desc    Add a stock
// @route   GET /api/v1/footballindex/players/:playerId/stocks
// @access  Private
exports.addStock = asyncHandler(async (req, res, next) => {
    req.body.player = req.params.playerId;
    req.body.user = req.user.id;

    const player = await Player.findById(req.params.playerId);
    if(!player){
        return next(new ErrorResponse(`No player with the id of ${req.params.playerId}`), 404);
    }


    // Make sure user is an admin
    if(req.user.role !== 'admin'){
        return next(
            new ErrorResponse(`User is not auth to add a stock to this player ${req.params.id}`, 404)
        );
    }

    const stock = await Stock.create(req.body);
    res.status(200).json({success: true, data: stock});
})


// @desc    Update a stock
// @route   PUT /api/v1/footballindex/stocks/:id
// @access  Private
exports.updateStock = asyncHandler(async (req, res, next) => {

    let stock = await Stock.findById(req.params.id);
    if(!stock){
        return next(new ErrorResponse(`No stock found by the id of ${req.pramas.id}`), 404 );
    }

    // Make sure user is an admin
    if(req.user.role !== 'admin'){
        return next(
            new ErrorResponse(`User is not auth to update a stock ${req.params.id}`, 404)
        );
    }

    stock = await Stock.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true
    });
    res.status(200).json({success: true, data: stock});
})




// @desc    Delete a stock
// @route   Delete /api/v1/footballindex/stocks/:id
// @access  Private
exports.deleteStock = asyncHandler(async (req, res, next) => {
    const stock = await Stock.findById(req.params.id);
    console.log(stock)
    if(!stock){
        return next(new ErrorResponse(`No stock found by the id of ${req.pramas.id}`), 404 );
    }
    // Make sure user si admin
    if(req.user.role !== 'admin'){
        return next(
            new ErrorResponse(`User is not auth to delete a stock ${req.params.id}`, 404)
        );
    }
    await stock.remove();
    res.status(200).json({success: true, data: {}});
})