const asyncHandler = require('../../middleware/async');
const ErrorResponse = require('../../utils/errorResponse');
const User = require('../../models/FootballIndex/Users');
const sendEmail = require('../../utils/sendEmail');
const crypto = require('crypto');

// @desc    Reg user
// @route   POST /api/v1/footballindex/auth
// @access  Public
exports.register = asyncHandler(async (req, res, next) => {
    const { name, email, password, role } = req.body
    console.log(req.body)
    // create user
    const user = await User.create({
        name,
        email,
        password,
        role
    });

    sendTokenResponse(user,200,res);

});


// @desc    Login user
// @route   POST /api/v1/footballindex/auth/login
// @access  Public
exports.login = asyncHandler(async (req, res, next) => {
    const { email, password } = req.body

    // Validate email and password
    if(!email || !password){
        return next(new ErrorResponse('Please provide an email and password', 400))
    }

    // Check for user
    const user = await User.findOne({email}).select('+password');

    if(!user){
        return next(new ErrorResponse('Invalid credentails', 401));
    }

   // Check if password matches
   const isMatch = await user.matchPassword(password) 

   if(!isMatch){
        return next(new ErrorResponse('Invalid credentails', 401));
    }

    sendTokenResponse(user,200,res);

})


// Get token from model, create cookie and send response
const sendTokenResponse = (user, statusCode , res) => {
    // Create token
    const token = user.getSignedJwtToken();
    let options = {}
    if(user.role === 'admin'){
        options = { 
            httpOnly: true
        };
    }else{
        options = { 
            expires: new Date(Date.now() + 30 * 24 * 60 * 60 * 1000),
            httpOnly: true
        };
    }


    // if(process.env.NODE_ENV === 'production'){
    //     options.secure = true;
    // }

    res
        .status(statusCode)
        .cookie('token', token, options)
        .json({
            success: true,
            token
        });
}



// @desc    Current logged in user
// @route   POST /api/v1/footballindex/auth/login
// @access  Private 
exports.getMe = asyncHandler(async(req, res, next) => {
    const user = await User.findById(req.user.id);

    res.status(200).json({
        sucess: true,
        data: user
    })
})


// @desc    Update user detail
// @route   PUT /api/v1/footballindex/auth/updatedetails
// @access  Private 
// TODO: retruning req.user as undefined thus not updating user..
exports.updateDetails = asyncHandler(async(req, res, next) => {
    const fieldToUpdate = {
        name: req.body.name,
        email: req.body.email
    }

    console.log(req.user)

    const user = await User.findByIdAndUpdate(req.user.id, fieldToUpdate, {
        new: true,
        runValidators: true
    });

    res.status(200).json({
        sucess: true,
        data: user
    })
})

// @desc    Udate password
// @route   PUT /api/v1/footballindex/auth/updatepassword
// @access  Private 
exports.updatePassword = asyncHandler(async(req, res, next) => {
    const user = await User.findById(req.user.id).select('+password');

    // Check current password
    if(!(await user.matchPassword(req.body.currentPassword))){
        return next(new ErrorResponse('Password is inccorect', 401));
    }

    user.password = req.body.newPassword
    await user.save()

    sendTokenResponse(user,200,res);

})


// @desc    Forgot Password
// @route   POST /api/v1/footballindex/auth/forgotpassword
// @access  Public 
exports.forgotPassword = asyncHandler(async(req, res, next) => {
    const user = await User.findOne({email: req.body.email});

    if(!user){
        return next(new ErrorResponse('This is no user with that email in the db', 404));
    }

    // Get rest token
    const resetToken = await user.getResetPasswordToken();

    await user.save({ validateBeforeSave: false });

    // Create rest url
    const resetUrl = `${req.protocol}://${req.get('host')}/api/v1/footballindex/auth/resetpassword/${resetToken}`;

    const message = `You are reving this email cos you have request the retst of a passwod. Please make a PUT request to \n\n ${resetUrl}`

    try {
        await sendEmail({
            email: user.email,
            subject: 'Password reset token',
            message
        })

        res.status(200).json({success: true, data: 'Email sent'})
    } catch (err) {
        console.log(err);
        user.resetPasswordToken = undefined;
        user.resetPasswordExpire = undefined;

        await user.save({ validateBeforeSave: false });

        return next(new ErrorResponse('Email could not be sent', 500))
    }

    res.status(200).json({
        sucess: true,
        data: user
    })
})

// @desc    Reset Password
// @route   PUT /api/v1/footballindex/auth/resetpassword/:resettoek
// @access  Public 
exports.resetPassword = asyncHandler(async(req, res, next) => {
    // Get hashed token 
    const resetPasswordToken = crypto.createHash('sha256').update(req.params.resettoken).digest('hex');
    const user = await User.findOne({
        resetPasswordToken,
        resetPasswordExpire: { $gt: Date.now() }
    });

    if(!user){
        return next(new ErrorResponse('Invaild Token', 404))
    }
    
   // Set new password
   user.password = req.body.password
   user.resetPasswordToken = undefined
   user.resetPasswordExpire = undefined;
   
   await user.save();
   sendTokenResponse(user, 200, res);

})


// A static is called on the model itslef wereas a method is called on the object