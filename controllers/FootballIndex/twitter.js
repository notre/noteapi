const Twitter = require('../../models/FootballIndex/Twitter');
const asyncHandler = require('../../middleware/async');
const ErrorResponse = require('../../utils/errorResponse');
const Player = require('../../models/FootballIndex/Player');

// @desc    Get all Twitters 
// @route   GET /api/v1/footballindex/tweets
// @route   GET /api/v1/footballindex/players/:playerId/tweets
// @access  Public
exports.getTweets = asyncHandler(async (req, res, next) => {
    if(req.params.playerId){
        const tweets = await Twitter.find({player: req.params.playerId})
        return res.status(200).json({
            success: true,
            count: tweets.length,
            data: tweets
        })
    }else{
        res.status(200).json(res.advancedResults);
    }
});

// @desc    Get a single Twitter 
// @route   GET /api/v1/footballindex/tweets/:id
// @access  Public
// TODO: Make this findable with twitter id too
exports.getTweet = asyncHandler(async (req, res, next) => {
    const tweet = await Twitter.findById(req.params.id)
    if(!tweet){
        return next(new ErrorResponse(`No tweet with the id of ${req.params.id}`), 404);
    }
    res.status(200).json({success: true, data: tweet});
})


// @desc    Add a tweet 
// @route   GET /api/v1/footballindex/players/:playerId/tweets
// @access  Private
exports.addTweet = asyncHandler(async (req, res, next) => {
    req.body.player = req.params.playerId;
    req.body.user = req.user.id;

    const player = await Player.findById(req.params.playerId);
    if(!player){
        return next(new ErrorResponse(`No player with the id of ${req.params.playerId}`), 404);
    }


    // Make sure user is an admin
    if(req.user.role !== 'admin'){
        return next(
            new ErrorResponse(`User is not auth to add a stock to this player ${req.params.id}`, 404)
        );
    }

    const tweet = await Twitter.create(req.body);
    res.status(200).json({success: true, data: tweet});
})

// @desc    Update a stock
// @route   PUT /api/v1/footballindex/tweets/:id
// @access  Private
exports.updateTweet = asyncHandler(async (req, res, next) => {

    let tweet = await Twitter.findById(req.params.id);
    if(!tweet){
        return next(new ErrorResponse(`No tweet found by the id of ${req.pramas.id}`), 404 );
    }

    // Make sure user is an admin
    if(req.user.role !== 'admin'){
        return next(
            new ErrorResponse(`User is not auth to update a tweet ${req.params.id}`, 404)
        );
    }

    tweet = await Twitter.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true
    });
    res.status(200).json({success: true, data: tweet});
})




// @desc    Delete a stock
// @route   Delete /api/v1/footballindex/tweets/:id
// @access  Private
exports.deleteTweet = asyncHandler(async (req, res, next) => {
    const tweet = await Twitter.findById(req.params.id);
    if(!tweet){
        return next(new ErrorResponse(`No tweet found by the id of ${req.pramas.id}`), 404 );
    }
    // Make sure user si admin
    if(req.user.role !== 'admin'){
        return next(
            new ErrorResponse(`User is not auth to delete a tweet ${req.params.id}`, 404)
        );
    }
    await tweet.remove();
    res.status(200).json({success: true, data: {}});
})



