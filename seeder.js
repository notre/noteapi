const fs = require('fs');
const mongoose = require('mongoose');
const colors = require('colors');
const dotenv = require('dotenv');

// Load enc vars
dotenv.config({path: './config/config.env'});

// Load modesl
const Player = require('./models/FootballIndex/Player');
const User = require('./models/FootballIndex/User');
const Stock = require('./models/FootballIndex/Stocks');
const Twitter = require('./models/FootballIndex/Twitter');

// Connect to DB
mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
});

// Read JSON File
const players = JSON.parse(fs.readFileSync(`${__dirname}/_data/players.json`, 'utf-8'));
const users = JSON.parse(fs.readFileSync(`${__dirname}/_data/users.json`, 'utf-8'));
const stocks = JSON.parse(fs.readFileSync(`${__dirname}/_data/stocks.json`, 'utf-8'));
const tweets = JSON.parse(fs.readFileSync(`${__dirname}/_data/twitter.json`, 'utf-8'));

// Import into DB

const importData = async (model) => {
    try {
        if(model === 'Player'){
            await Player.create(players);
        }else if(model === 'All'){
            await Player.create(players);
            await User.create(users); 
            await Stock.create(stocks);
        }else if(model === 'User'){
            await User.create(users); 
        }else if(model === 'Stock'){
            await Stock.create(stocks);
        }else if(model === 'Twitter'){
            await Twitter.create(tweets)
        }
        console.log('Data Imported...'.green.inverse);
        process.exit();
    } catch (err) {
        console.log(err);
    }
}

// Delete Data
const deleteData = async (model) => {
    try {
        if(model === 'Player'){
            await Player.deleteMany();
        }else if(model === 'All'){
            await Player.deleteMany();
            await User.deleteMany();
            await Stock.deleteMany();
        }else if(model === 'User'){
            await User.deleteMany();
        }else if(model === 'Stock')
        console.log('Data deleted...'.red.inverse);
        process.exit();
    } catch (err) {
        console.log(err);
    }
}

// Print all data
const printAllData = async (model) => {
    try {
        if(model === 'Player'){
            const data = await Player.find();
        }else if(model === 'All'){
            const data1 = await Player.find();
        }
        console.log('Printing Data...'.green.inverse);
        console.log(data)
        process.exit();
    } catch (err) {
        console.log(err);
    }
}


// Print all data - need to change it to slug!!
const printDataById = async (id, model) => {
    try {
        if(model === 'Player'){
            const data = await Player.findById(id);
        }else if(model === 'User'){
            const data = await User.findById(id);
        }
        console.log('Printing Data...'.green.inverse);
        console.log(data)
        process.exit();
    } catch (err) {
        console.log(err);
    }
}

if(process.argv[2] === '-import'){
    importData(process.argv[3]);
}else if(process.argv[2] === '-delete'){
    deleteData(process.argv[3]);
}else if(process.argv[2] === '-printAll'){
    printAllData(process.argv[3]);
}else if(process.argv[2] === '-print' && process.argv[3]){
    printDataById(process.argv[3], process.argv[4]);
}else{
    console.log('Enter an arguemnet -import, -delete, -printAll, -print $id');
}
