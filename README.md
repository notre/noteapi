# FUlly functional API

## Tech used

- Node.js
- Express for routing
- MongoDB for DB
- JWT for auth
- Azure for hosting
- Mongoose for DB modling in js

## How to run

1. PROD
2. UAT
  
## Code Structure

### Express

1. sever.js file is the inital file.
2. Each endpoint has its own function.
   1. In routes the type of reuqtes is defined ie PUT, GET etc and what function is ran
   2. In controllers the functions methods lie

### Enviremental vars

1. Projects env vars are stored in config/config.env
2. They are accessable though a lib called dotenv
   1. One can access the env var by `rocess.env.${VAR}`

### Mongoose

can be found in models/Player.js

Modeled in on object schema. Easy vaildation checking

### ERROR Handling

How did I do it?

### Libs used:

### Virtuals

These are doucment propertues that you can get and set that do not get presisted to the mongo DB


### Quering with express

- You can query the db though the url.
- After the url the first char is a ? then the key you are searching for and then an & and the value.
- Each key and value pair after that are sperated by an &.
- url?select=name,description (this will return only the name and description feilds)
- if you have objcets you can use url?object.insideObject=value
- If you have a number or a date you can use less than, greater them ect. 
  - url?stock[gt]=10 (this is stock greater then 10)

### DB quering editing the URL
- select
- sort
- paginiatino

### Architecture

![](Notre&#32;API&#32;arch.jpg)

#### Notes

##### What is Middleware?

This is a fuction that has a access to the request response cycle and has access to the cycle.
It in the middle of a request. 

A Middleware takes in a req, res and next

```javascript
const logger = (req, res, next ) => { 
    req.hello = 'Hello World';
    console.log('Middleware ran');
    // This tells it to move on to the next peacse of middleware
    next();
}

app.use(logger);

```

You can then use this within the router.


##### Error handling

If there are any promise errors a message is show and the app exits with exit code 1.

This is in server.js


```javascript
// Handle unhanded rejections
process.on('unhandledRejection', (err, promise) => {
    console.log(`ERROR ${err.message}`);
    // Close and exit
    server.close(() => {
        process.exit(1);
    });
})
```

##### Used nice conosle line color and bolding etc.

Lib called colors.

After the text just add .$color or .$italic etc.




https://www.npmjs.com/package/bcryptjs

https://jwt.io/

https://github.com/auth0/node-jsonwebtoken


https://www.npmjs.com/package/cookie-parser

use this to store the token

A static is called on the model itslef wereas a method is called on the object

mailtrap
https://mailtrap.io/register/signup?ref=hero

Node mailer
https://nodemailer.com/about/