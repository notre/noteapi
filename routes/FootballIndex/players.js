
const express = require('express');
const {getPlayer, getPlayers, createPlayer, deletePlayer, playerPhotoUpload, updatePlayer, getPlayerStockValues} = require('../../controllers/FootballIndex/players')
const stocksRouter = require('./stocks');
const tweetsRouter = require('./twitter');

// inclued othe rresourse routers
const advancedResults = require('../../middleware/advancedResults');
const Player = require('../../models/FootballIndex/Player');

const router = express.Router();

const { protect } = require('../../middleware/auth');

// Re-route into othe resource routers
router.use('/:playerId/stocks', stocksRouter)
router.use('/:playerId/tweets', tweetsRouter)

router.route('/').get(advancedResults(Player),getPlayers).post(protect, createPlayer);
router.route('/:id').get(getPlayer).put(protect,updatePlayer).delete(protect,deletePlayer);
router.route('/:id/photo').put(protect, playerPhotoUpload);
router.route('/:id/stockvalues').get(getPlayerStockValues)



module.exports = router;
