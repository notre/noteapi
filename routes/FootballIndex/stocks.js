const express = require('express');
const {getStocks, getStock ,addStock, updateStock, deleteStock} = require('../../controllers/FootballIndex/stocks')

// need mergeParams cos we are mergein the URL params with player
const router = express.Router({ mergeParams: true} );
const Stock = require('../../models/FootballIndex/Stocks');
const advancedResults = require('../../middleware/advancedResults');
const { protect } = require('../../middleware/auth');


router.route('/').get(advancedResults(Stock, {
    path: 'player',
    select: 'name'
}),getStocks).post(protect, addStock);


router.route('/:id').get(getStock).put(protect, updateStock).delete(protect ,deleteStock);


module.exports = router;
