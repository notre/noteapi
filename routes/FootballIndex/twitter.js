const express = require('express');
const {getTweets, getTweet, addTweet, deleteTweet, updateTweet} = require('../../controllers/FootballIndex/twitter')

// need mergeParams cos we are mergein the URL params with player
const router = express.Router({ mergeParams: true} );
const Tweet = require('../../models/FootballIndex/Twitter');
const advancedResults = require('../../middleware/advancedResults');
const { protect } = require('../../middleware/auth');

router.route('/').get(advancedResults(Tweet, {
    path: 'player',
    select: 'name'
}),getTweets).post(protect, addTweet);


router.route('/:id').get(getTweet).delete(protect ,deleteTweet).put(protect, updateTweet);


module.exports = router;