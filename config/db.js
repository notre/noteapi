const mongoose = require('mongoose');

const connectDB = async () => {
    const conn = await mongoose
    .connect(
      'mongodb://mongo:27017/docker-node-mongo',
      { useNewUrlParser: true }
    )

    console.log(`MongoDB Connected: ${conn.connection.host}`.cyan.underline.bold);

}

module.exports = connectDB;